import os
from tkinter import *
print(os.environ)
def COMPUTERNAME():
    lbl.configure(text=os.environ["COMPUTERNAME"])
def OS():
    lbl.configure(text=os.environ["OS"])
def PROCESSOR_ARCHITECTURE():
    lbl.configure(text=os.environ["PROCESSOR_ARCHITECTURE"])
def LOCALAPPDATA():
    lbl.configure(text=os.environ["LOCALAPPDATA"])

window = Tk()
window.title("Информация об ОС")
window.geometry('500x200')
lbl = Label(window)
btn = Button(window, text="COMPUTERNAME", command=COMPUTERNAME)
btn1 = Button(window, text="OS", command=OS)
btn2 = Button(window, text="PROCESSOR_ARCHITECTURE", command=PROCESSOR_ARCHITECTURE)
btn3 = Button(window, text="LOCALAPPDATA", command=LOCALAPPDATA)
btn.grid(column = 0, row = 0)
btn1.grid(column = 2, row = 0)
btn2.grid(column = 1, row = 0)
btn3.grid(column = 3, row = 0)
lbl.grid(column = 1, row = 1)
window.mainloop()


